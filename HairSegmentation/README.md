# Deep Dye Segmentation Code
## The Jupyter notebook doubles as a tutorial. To donwload the dataset, just uncomment the first two lines.

To use the libraries you need to download the following packages:
* Fastai
* Pytorch
* Numpy

Preferably use gpu to train the model since it is much faster. To check if you are using GPU or not, make a new cell and enter "torch.cuda.is_available()" which should return True.

Server side implementation at https://github.com/alcros33/DeepDYE
Application side implementation at --- missing ---

# Showcase
These are some the the images that we used to test out the model with the app.

![alt text][Original]
![alt text][Pink]
![alt text][Blond]

[Original]: Images/Original.png "Original"
[Pink]: Images/Pink.png "Pink"
[Blond]: Images/Blond.png "Blond"
