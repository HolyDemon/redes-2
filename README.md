# Redes 2

  Independent projects for the course "Redes 2: Revenge of the Convolutions" in UNAM ENES Campus Morelia.

 [Course GitHub](https://github.com/mraggi/NeuralSensei)
 
## CONTRIBUTING

Since this is course work pull requests would not be accepted, but if you want to work on it by yourself feel free to fork the project, but I would recommend going through the course linked above.
### TECH STACK
- Conda
- Fastai
- Jupyter
- PyTorch

## List
1. Flower Classification
   1. Dataset from [here.](http://www.robots.ox.ac.uk/~vgg/data/flowers/)
   1. First Article from [here.](https://www.robots.ox.ac.uk/~vgg/research/flowers_demo/docs/Chai11.pdf)
   1. Second Article from [here.](www.cs.huji.ac.il/~daphna/IsraeliFlowers/downloads/Super-Fine-Grained-Flower-Based-Classification.pdf)
1. Pet Classification
   1. Dataset from [here.](http://www.robots.ox.ac.uk/~vgg/data/pets/data/images.tar.gz)
